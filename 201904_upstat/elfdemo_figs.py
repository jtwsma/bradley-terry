#!/usr/bin/env python3
#
# Copyright (C) 2020 John T. Whelan
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
This script generates some of the plots for Whelan and Wodon,
"Prediction and Evaluation in College Hockey Using the
Bradley-Terry-Zermelo Model" (2020), to appear in Mathematics for
Applications.  These plots are part of a demonstration inspired by
a discussion on the eLynah Forum:
http://elf.elynah.com/read.php?1,213740
Additional plots are created by btprediction_figs.py
"""

__author__ = "John Whelan <jtw24@cornell.edu>"

import matplotlib
matplotlib.use("Agg")
from matplotlib import rc
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from scipy import stats
from scipy.special import expit, logit

##############################################################################
# read in the teams file

teams = np.genfromtxt('elfdemo_teams.dat',dtype=None,encoding=None)
nteams = len(teams)
teamind = dict([(t,i) for i,t in enumerate(teams)])
MAPlambda_t = np.loadtxt('elfdemo_MAP_lambda.dat',unpack=True)
MAPsigsq_tt = np.loadtxt('elfdemo_MAP_sigsq.dat')
lambdaCr = MAPlambda_t[teamind['Cr']]
lambdaQn = MAPlambda_t[teamind['Qn']]
sigsqCrCr = MAPsigsq_tt[teamind['Cr'],teamind['Cr']]
sigsqCrQn = MAPsigsq_tt[teamind['Cr'],teamind['Qn']]
sigsqQnQn = MAPsigsq_tt[teamind['Qn'],teamind['Qn']]
sigmaCr = np.sqrt(sigsqCrCr)
sigmaQn = np.sqrt(sigsqQnQn)

## Gaussian simulation

gausslnimpnumer_jI = np.loadtxt('elfdemo_lnimpnumer.dat',unpack=True)
gausslnimpdenom_jI = np.loadtxt('elfdemo_lnimpdenom.dat',unpack=True)
Niter = len(gausslnimpnumer_jI)
Nrep = 4
if (Niter % Nrep):
    raise(
        ValueError,
        ('Number of replications %d does not divide number of iterations %d evenly'
         % (Nrep,Niter))
    )
Niter = Niter // Nrep
gausslnimpnumer_jI = gausslnimpnumer_jI.reshape(Nrep,Niter)
gausslnimpdenom_jI = gausslnimpdenom_jI.reshape(Nrep,Niter)
gaussweights_jI = np.exp(gausslnimpnumer_jI-gausslnimpdenom_jI)
gaussweights_jI /= np.sum(gaussweights_jI,axis=-1)[:,None]

lambdasimCr_jI,lambdasimQn_jI = np.loadtxt('elfdemo_lambdasim.dat',
                                     usecols=(teamind['Cr'],teamind['Qn']),
                                     unpack=True)
lambdasimCr_jI = lambdasimCr_jI.reshape(Nrep,Niter)
lambdasimQn_jI = lambdasimQn_jI.reshape(Nrep,Niter)
hhwpsimCrQn_jI = expit(lambdasimCr_jI - lambdasimQn_jI)
np.random.seed(19702900)
gameone_jI = stats.bernoulli.rvs(hhwpsimCrQn_jI)
gametwo_jI = stats.bernoulli.rvs(hhwpsimCrQn_jI)
gamethree_jI = stats.bernoulli.rvs(hhwpsimCrQn_jI)
series_jI = ( gameone_jI + gametwo_jI + gamethree_jI >= 2)

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
# lammin = lambdaQn - 3*sigmaQn
# lammax = lambdaCr + 3*sigmaCr
lammin = np.log(20./100.)
lammax = np.log(2000./100.)
lamplot = np.linspace(lammin,lammax,100)
fmax = 1.2*stats.norm(loc=lambdaQn,scale=sigmaQn).pdf(lambdaQn)
ax.plot(lamplot,stats.norm(loc=lambdaCr,scale=sigmaCr).pdf(lamplot),
        'r-',label='Cornell')
ax.plot([lambdaCr,lambdaCr],[0,fmax],'r:')
ax.plot(lamplot,stats.norm(loc=lambdaQn,scale=sigmaQn).pdf(lamplot),
        'b--',label='Quinnipiac')
ax.plot([lambdaQn,lambdaQn],[0,fmax],'b:')
ax.legend()
tickvals = np.array([2,5,10,20,50,100,200,500,1000,2000,5000])
ticklabs = ['$%d$' % t for t in tickvals]
ticklogs = np.log(tickvals/100)
ax.set_xticks(ticklogs)
ax.set_xticklabels(ticklabs)
ax.set_yticks([])
ax.set_xlabel('Strength')
ax.set_ylabel('probability density')
ax.set_xlim([lammin,lammax])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('Cr.eps',bbox_inches='tight')

sigsq_MM = np.array([[sigsqCrCr,sigsqCrQn],[sigsqCrQn,sigsqQnQn]])
invsigsq_MM = np.linalg.inv(sigsq_MM)
lam = np.linspace(lammin,lammax,1000)
lam1 = np.outer(lam,np.ones_like(lam))
lam2 = np.outer(np.ones_like(lam),lam)
P2d = np.exp(-((lam1-lambdaCr)*(lam1-lambdaCr)*invsigsq_MM[0,0]
               +(lam1-lambdaCr)*(lam2-lambdaQn)*invsigsq_MM[0,1]
               +(lam2-lambdaQn)*(lam1-lambdaCr)*invsigsq_MM[1,0]
               +(lam2-lambdaQn)*(lam2-lambdaQn)*invsigsq_MM[1,1])/2)

fig = plt.figure();
fig.set_size_inches(4,4);

ax = fig.add_subplot(111)
ax.contour(lam1,lam2,P2d,colors='k',linewidths=2)
ax.plot([lambdaCr,lambdaCr],[lammin,lammax],'r:')
ax.plot([lammin,lammax],[lambdaQn,lambdaQn],'b:')
ax.set_xticks(ticklogs)
ax.set_xticklabels(ticklabs)
ax.set_yticks(ticklogs)
ax.set_yticklabels(ticklabs)
# ax.set_xlim([lammin,lammax])
# ax.set_ylim([lammin,lammax])
ax.set_xlim([np.log(100./100.),np.log(2000./100.)])
ax.set_ylim([np.log(20./100.),np.log(400./100.)])
ax.set_xlabel('Cornell strength')
ax.set_ylabel('Quinnipiac strength')
ax.grid(True)
fig.savefig('CrQn.eps',bbox_inches='tight')

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
print ('Cornell/Quinnipiac KRACH odds are %.2f:1' % np.exp(lambdaCr-lambdaQn))
lammin = np.log(1./2.)
lammax = np.log(32./1.)
lamplot = np.linspace(lammin,lammax,100)
lampdf = stats.norm(loc=lambdaCr-lambdaQn,
                           scale=np.sqrt(sigsqCrCr+sigsqQnQn-2*sigsqCrQn)
                       ).pdf(lamplot)
fmax = 1.2*max(lampdf)
ax.plot(lamplot,lampdf,'k-')
ax.plot([lambdaCr-lambdaQn,lambdaCr-lambdaQn],[0,fmax],'g:',
        label='KRACH estimate')
ax.legend()
tickvals = [(1,4),(1,2),(1,1),(2,1),(4,1),(8,1),(16,1),(32,1)]
ticklabs = [('$%d:%d$' % t) for t in tickvals]
ticklogs = np.log(np.array([t[0]/t[1] for t in tickvals]))
ax.set_xticks(ticklogs)
ax.set_xticklabels(ticklabs)
ax.set_yticks([])
ax.set_xlabel('Cornell v Quinnipiac odds')
ax.set_ylabel('probability density')
ax.set_xlim([lammin,lammax])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('CrQnratio.eps',bbox_inches='tight')

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
# probplot = expit(lamplot)
probplot = np.linspace(0.0001,.9999,10000)[1:-1]
lamplot = logit(probplot)
lampdf = stats.norm(loc=lambdaCr-lambdaQn,
                           scale=np.sqrt(sigsqCrCr+sigsqQnQn-2*sigsqCrQn)
                       ).pdf(lamplot)
probpdf = lampdf/(probplot*(1-probplot))
fmax = 1.2*max(probpdf)
ax.plot(probplot,probpdf,'k-')
pkrach = expit(lambdaCr-lambdaQn)
ax.plot([pkrach,pkrach],[0,fmax],'g:',
        label='KRACH estimate')
pmarg = sum(probpdf*probplot)/sum(probpdf)
ax.plot([pmarg,pmarg],[0,fmax],'m-.',
        label='posterior prediction')
ax.legend()
ax.set_yticks([])
ax.set_xlabel('Cornell v Quinnipiac probability')
ax.set_ylabel('probability density')
ax.set_title('Posterior Distribution (Gaussian approx)')
ax.set_xlim([0,1])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('CrQnprob.eps',bbox_inches='tight')

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
for j in range(Nrep):
    dens,bins = np.histogram(hhwpsimCrQn_jI[j],bins=100,density=True)
    ax.step(bins[1:],dens,where='pre',color=('C%d' % j))

pavg_j = np.mean(hhwpsimCrQn_jI,axis=-1)
pmc_j = np.mean(gameone_jI,axis=-1)
pavgimp_j = np.sum(hhwpsimCrQn_jI*gaussweights_jI,axis=-1)
pmcimp_j = np.sum(gameone_jI*gaussweights_jI,axis=-1)

ax.plot(probplot,probpdf,'k-')
ax.plot([pkrach,pkrach],[0,fmax],'g:',
        label='KRACH estimate')
ax.plot([pmarg,pmarg],[0,fmax],'m-.',
        label='posterior prediction')
for j in range(Nrep):
    ax.plot([pavg_j[j],pavg_j[j]],[0,fmax],color=('C%d' % j),lw=0.5)
    ax.plot([pmc_j[j],pmc_j[j]],[0,fmax],color=('C%d' % j),lw=0.5,ls='--')
ax.legend()
ax.set_yticks([])
ax.set_title('Posterior Distribution (Gaussian Monte Carlo)')
ax.set_xlabel('Cornell v Quinnipiac probability')
ax.set_ylabel('probability density')
ax.set_xlim([0,1])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('mcCrQnprob.eps',bbox_inches='tight')

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
for j in range(Nrep):
    dens,bins = np.histogram(hhwpsimCrQn_jI[j],weights=gaussweights_jI[j],
                             bins=100,density=True)
    ax.step(bins,np.concatenate((dens,[0.])),
            where='post',color=('C%d' % j))

ax.plot(probplot,probpdf,'k-',
        label='Gaussian posterior approximation')
ax.plot([pkrach,pkrach],[0,fmax],'g--',
        label='KRACH estimate')
ax.plot([pmarg,pmarg],[0,fmax],'m-.',
        label='Gaussian posterior prediction')
for j in range(Nrep):
    if (j==0):
        ax.plot([pmcimp_j[j],pmcimp_j[j]],[0,fmax],
                color='#888888',ls=':',lw=1,
                label='importance sampling prediction')
    else:
        ax.plot([pmcimp_j[j],pmcimp_j[j]],[0,fmax],
                color='#888888',ls=':',lw=1)
ax.legend(handlelength=2.5)
ax.set_yticks([])
ax.set_title('Posterior Distribution')
ax.set_xlabel('Cornell v Quinnipiac probability')
ax.set_ylabel('probability density')
ax.set_xlim([0,1])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('gaussimpCrQnprob.eps',bbox_inches='tight')

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
seriesplot = 3*probplot**2 - 2*probplot**3
seriespdf = probpdf / (6*probplot*(1-probplot))
skrach = 3*pkrach**2 - 2*pkrach**3
fmax = 1.2*max(seriespdf)
ax.plot(seriesplot,seriespdf,'k-')
ax.plot([skrach,skrach],[0,fmax],'g:',
        label='KRACH estimate')
smarg = sum(probpdf*seriesplot)/sum(probpdf)
ax.plot([smarg,smarg],[0,fmax],'m-.',
        label='posterior prediction')
ax.set_yticks([])
ax.legend()
ax.set_title('Posterior Distribution (Gaussian approx)')
ax.set_xlabel('Cornell v Quinnipiac best-of-3 series probability')
ax.set_ylabel('probability density')
ax.set_xlim([0,1])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('CrQnseries.eps',bbox_inches='tight')

b3wpsimCrQn_jI = 3*hhwpsimCrQn_jI**2 - 2*hhwpsimCrQn_jI**3
savg_j = np.mean(b3wpsimCrQn_jI,axis=-1)
smc_j = np.mean(series_jI,axis=-1)
savgimp_j = np.sum(b3wpsimCrQn_jI*gaussweights_jI,axis=-1)
smcimp_j = np.sum(series_jI*gaussweights_jI,axis=-1)

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
for j in range(Nrep):
    dens,bins = np.histogram(b3wpsimCrQn_jI[j],bins=100,density=True)
    ax.step(bins[1:],dens,where='pre')

ax.plot(seriesplot,seriespdf,'k-')
ax.plot([skrach,skrach],[0,fmax],'g:',
        label='KRACH estimate')
ax.plot([smarg,smarg],[0,fmax],'m-.',
        label='posterior prediction')
for j in range(Nrep):
    ax.plot([savg_j[j],savg_j[j]],[0,fmax],color=('C%d' % j),lw=0.5)
    ax.plot([smc_j[j],smc_j[j]],[0,fmax],color=('C%d' % j),lw=0.5,ls='--')
ax.legend()
ax.set_yticks([])
ax.set_title('Posterior Distribution (Gaussian Monte Carlo)')
ax.set_xlabel('Cornell v Quinnipiac series probability')
ax.set_ylabel('probability density')
ax.set_xlim([0,1])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('mcCrQnseries.eps',bbox_inches='tight')

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)
for j in range(Nrep):
    dens,bins = np.histogram(b3wpsimCrQn_jI[j],weights=gaussweights_jI[j],
                             bins=100,density=True)
    ax.step(bins,np.concatenate((dens,[0.])),
            where='post',color=('C%d' % j))

ax.plot(seriesplot,seriespdf,'k-',
        label='Gaussian posterior approximation')
ax.plot([skrach,skrach],[0,fmax],'g--',
        label='KRACH estimate')
ax.plot([smarg,smarg],[0,fmax],'m-.',
        label='Gaussian posterior prediction')
for j in range(Nrep):
    if (j==0):
        ax.plot([smcimp_j[j],smcimp_j[j]],[0,fmax],
                color='#888888',ls=':',lw=1,
                label='importance sampling prediction')
    else:
        ax.plot([smcimp_j[j],smcimp_j[j]],[0,fmax],
                color='#888888',ls=':',lw=1)
ax.legend(handlelength=2.5)
ax.set_yticks([])
ax.set_title('Posterior Distribution')
ax.set_xlabel('Cornell v Quinnipiac probability')
ax.set_ylabel('probability density')
ax.set_xlim([0,1])
ax.set_ylim([0,fmax])
ax.grid(True)
fig.savefig('gaussimpCrQnseries.eps',bbox_inches='tight')

datfile = open('elfdemo_data.tex','w')
datfile.write(r'\newcommand{\elfCrKRACH}{%.1f}'
              % (100*np.exp(lambdaCr))
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfQnKRACH}{%.2f}'
              % (100*np.exp(lambdaQn))
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnKRACHpct}{%.1f}'
              % (100*pkrach)
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQngausspct}{%.1f}'
              % (100*pmarg)
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgpcta}{%.1f}'
              % (100*pavg_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgpctb}{%.1f}'
              % (100*pavg_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgpctc}{%.1f}'
              % (100*pavg_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgpctd}{%.1f}'
              % (100*pavg_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcpcta}{%.1f}'
              % (100*pmc_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcpctb}{%.1f}'
              % (100*pmc_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcpctc}{%.1f}'
              % (100*pmc_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcpctd}{%.1f}'
              % (100*pmc_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimppcta}{%.1f}'
              % (100*pavgimp_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimppctb}{%.1f}'
              % (100*pavgimp_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimppctc}{%.1f}'
              % (100*pavgimp_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimppctd}{%.1f}'
              % (100*pavgimp_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimppcta}{%.1f}'
              % (100*pmcimp_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimppctb}{%.1f}'
              % (100*pmcimp_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimppctc}{%.1f}'
              % (100*pmcimp_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimppctd}{%.1f}'
              % (100*pmcimp_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnKRACHseriespct}{%.1f}'
              % (100*skrach)
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQngaussseriespct}{%.1f}'
              % (100*smarg)
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgseriespcta}{%.1f}'
              % (100*savg_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgseriespctb}{%.1f}'
              % (100*savg_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgseriespctc}{%.1f}'
              % (100*savg_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgseriespctd}{%.1f}'
              % (100*savg_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcseriespcta}{%.1f}'
              % (100*smc_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcseriespctb}{%.1f}'
              % (100*smc_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcseriespctc}{%.1f}'
              % (100*smc_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcseriespctd}{%.1f}'
              % (100*smc_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimpseriespcta}{%.1f}'
              % (100*savgimp_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimpseriespctb}{%.1f}'
              % (100*savgimp_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimpseriespctc}{%.1f}'
              % (100*savgimp_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnavgimpseriespctd}{%.1f}'
              % (100*savgimp_j[3])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimpseriespcta}{%.1f}'
              % (100*smcimp_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimpseriespctb}{%.1f}'
              % (100*smcimp_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimpseriespctc}{%.1f}'
              % (100*smcimp_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfCrQnmcimpseriespctd}{%.1f}'
              % (100*smcimp_j[3])
              )
datfile.write('\n')
maxweight_j = np.max(gaussweights_jI,axis=-1)
datfile.write(r'\newcommand{\elfmaxweighta}{%.5f}'
              % (maxweight_j[0])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfmaxweightb}{%.5f}'
              % (maxweight_j[1])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfmaxweightc}{%.5f}'
              % (maxweight_j[2])
              )
datfile.write('\n')
datfile.write(r'\newcommand{\elfmaxweightd}{%.5f}'
              % (maxweight_j[3])
              )
datfile.write('\n')

datfile.close()
print(np.max(gaussweights_jI,axis=-1))
print(np.sort(gaussweights_jI,axis=-1))
