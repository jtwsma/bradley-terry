#!/usr/bin/env python3
#
# Copyright (C) 2020 John T. Whelan
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""This script reads in a set of game results, divided into "training"
and "evaluation" data using user-specified dates.  It uses the
training results, along with a specified prior, to compute the MAP
estimates of the Bradley-Terry log-strengths, and the associated
Hessian matrix, to produce a Gaussian approximation to the posterior.
It then computes the posterior predictive probability for the
evaluation results using either the Gaussian approximation or
importance sampling.  Importance sampling can be done with the
Gaussian or multivariate-t distribution, although the latter is not
recommended.  The posterior predictive probability is recorded in the
form of the log2-Bayes factor between the Bradley-Terry model and a
model which assigns a predictive probability of 0.5 to all game
results.  The script can also output the MAP estimates, Hessian
matrix, Monte Carlo samples, and importance weights, if requested.

"""

__author__ = "John Whelan <jtw24@cornell.edu>"

import numpy as np
from argparse import ArgumentParser
from scipy.special import expit, logit
from scipy.optimize import fsolve
from scipy import stats

def findConnections(h2hpf_tt):
    ## Figure out whether there are chains of wins (or ties) connecting
    ## each pair of teams.  This is important for the existence of the
    ## maximum-likelihood estimates; if this matrix is all 1s, everything
    ## is normal, but if there are some 0s, there will be pairs of teams
    ## for which the predicted winning percentage is automatically 0, 0.5
    ## or 1.  Each set of teams for which there is a connection in both
    ## directions makes a group (equivalence class in mathematical terms)
    ## See Butler and Whelan, arXiv:math.ST/0412232
    connections0_tt = np.eye(len(h2hpf_tt),dtype=int)
    connections_tt = connections0_tt + 1*(h2hpf_tt>0)

    while np.any(connections_tt != connections0_tt):
        connections0_tt = connections_tt
        connections_tt = 1*(np.dot(connections_tt,connections_tt)>0)

    return connections_tt

def FordMLE(h2hpf_tt,connections_tt=None,tol=1e-5):
    ## Use Ford's method to find the MLE
    ## When finding the maximum likelihood estimates, we don't want to
    ## consider games between teams which we know will end up with
    ## infinite or arbitrary KRACH ratios.
    if connections_tt is None:
        groupmask_tt = np.ones_like(h2hpf_tt,dtype=bool)
    else:
        groupmask_tt = np.logical_and(connections_tt,connections_tt.transpose())

    h2hpfingroup_tt = groupmask_tt * h2hpf_tt
    h2hpaingroup_tt = h2hpfingroup_tt.transpose()
    h2hnumingroup_tt = h2hpfingroup_tt + h2hpaingroup_tt
    ## Total number of game points for each team against teams in the same
    ## group
    pfingroup_t = h2hpfingroup_tt.sum(axis=1)
    paingroup_t = h2hpaingroup_tt.sum(axis=1)
    numingroup_t = pfingroup_t + paingroup_t
    nteams = len(h2hpf_tt)

    ## Find the maximum likelihood estimates for the log-(KRACH/100) values
    ## (which are the logarithms of what we've been using for KRACH/100)
    lambdaMLE0_t = np.zeros(nteams)
    ### If a group consists of only one team, it will have no points for
    ### or against; otherwise it's guaranteed to have at least one point
    ### for and against within the group
    epsilon = 1e-24
    pfcalc_t = pfingroup_t + epsilon
    pacalc_t = paingroup_t + epsilon
    lambdaMLE_t = np.log(pfcalc_t/pacalc_t)

    ## Loop until we have convergence.  Note this will happen immediately
    ## if every team is .500 within its group, but in that case the MLEs
    ## will indeed all be equal within a group.
    while max(np.abs(lambdaMLE_t-lambdaMLE0_t) > tol):
        lambdaMLE0_t = lambdaMLE_t
        piMLE_t = np.exp(lambdaMLE_t)
        lambdaMLE_t = np.log( pfcalc_t
                            /
                            ( epsilon
                              + (
                                  h2hnumingroup_tt
                                  /(piMLE_t[:,None]+piMLE_t[None,:])
                              ).sum(axis=1)
                            )
        )
        ## use groupmask_tt to subtract off the average lambdaMLE_t in the group
        lambdaMLE_t -= np.array([lambdaMLE_t[groupmask_tt[t]].mean()
                               for t in range(nteams)])

    return lambdaMLE_t

def MLEqnWithSigma(lam_t,pf_t,h2hnum_tt,sigma):
    ## Expression which vanishes if we're at the MAP point with a
    ## Gaussian prior
    return (
        pf_t - lam_t/sigma**2
        - (h2hnum_tt*expit(lam_t[:,None]-lam_t[None,:])).sum(axis=-1)
    )

def FordMAPWithSigma(h2hpf_tt,sigma,lambdaMAP_t=None,tol=1e-5):
    ## Find the MAP with a Gaussian prior
    ## Actually don't use Ford's method; use scipy.optimize.fsolve
    h2hpa_tt = h2hpf_tt.transpose()
    h2hnum_tt = h2hpf_tt + h2hpa_tt
    pf_t = h2hpf_tt.sum(axis=-1)
    if lambdaMAP_t is None:
        lambdaMAP_t = FordMLE(h2hpf_tt,tol=tol)

    lambdaMAP_t = fsolve(MLEqnWithSigma,lambdaMAP_t,
                         args=(pf_t,h2hnum_tt,sigma))
    return lambdaMAP_t

def FordMAPWithEta(h2hpf_tt,eta,tol=1e-5):
    ## Use a Ford-like method to find the MAP with a GL prior
    h2hpa_tt = h2hpf_tt.transpose()
    h2hnum_tt = h2hpf_tt + h2hpa_tt
    pf_t = h2hpf_tt.sum(axis=-1)
    pa_t = h2hpa_tt.sum(axis=-1)
    nteams = len(pf_t)
    pfcalc_t = pf_t + 2. * eta
    pacalc_t = pa_t + 2. * eta

    lambdaMAP0_t = np.zeros(nteams)
    lambdaMAP_t = np.log(pfcalc_t/pacalc_t)

    ## Loop until we have convergence.  Note this will happen
    ## immediately if every team is .500, but in that case all lambdas
    ## = 0 is indeed a solution to the MAP equations.
    while max(np.abs(lambdaMAP_t-lambdaMAP0_t) > tol):
        # print(lambdaMAP_t)
        lambdaMAP0_t = lambdaMAP_t
        piMAP_t = np.exp(lambdaMAP_t)
        oneminuszetaMAP_t = expit(-lambdaMAP_t)
        lambdaMAP_t = np.log(
            pfcalc_t
            /
            (
                ( h2hnum_tt/(piMAP_t[:,None]+piMAP_t[None,:]) ).sum(axis=1)
                + 4. * eta * oneminuszetaMAP_t
            )
        )

    return lambdaMAP_t

def h2hpctFromLambdas(lambda_t,connections_tt=None):
# Function to calculate expected head-to-head win percentage
# Inputs:
#       lamdas = vector of log-BT-team-strengths
#       connections_tt = matrix of comparable connections
#
# If connections_tt[i,j]==1==connections_tt[j,i], use the BT strengths
# If connections_tt[i,j]==1 and connections_tt[j,i]==0, h2hpct_tt[i,j]=1.
# If connections_tt[i,j]==0==connections_tt[j,i], h2hpct_tt[i,j]=0.5
    h2hpct_tt = 1. / (1. + np.exp(lambda_t[None,:]-lambda_t[:,None]))
    if connections_tt is not None:
        samegroup = connections_tt*connections_tt.transpose()
        diffgroup = 1 - samegroup
        h2hpct_tt = (
            samegroup * h2hpct_tt
            +
            diffgroup * 0.5 * ( 1 + connections_tt - connections_tt.transpose() )
        )
    return h2hpct_tt

ap = ArgumentParser(description=__doc__)
ap.add_argument("--scores-file", action="store",
                default="scores.dat",
                help='File game scores in tbrw format')
ap.add_argument("--first-training-date", action="store", type=int,
                default=10000101,
                help='Date of first game of training set in YYYYMMDD format')
ap.add_argument("--final-training-date", action="store", type=int,
                default=30000101,
                help='Date of final game of training set in YYYYMMDD format')
ap.add_argument("--first-eval-date", action="store", type=int,
                default=10000101,
                help='Date of first game of evaluation set in YYYYMMDD format')
ap.add_argument("--final-eval-date", action="store", type=int,
                default=30000101,
                help='Date of final game of evaluation set in YYYYMMDD format')
ap.add_argument("--prior-sigma", action="store", type=float,
                default=np.inf,
                help='Use a Gaussian prior on log-strengths with sd sigma')
ap.add_argument("--prior-eta", action="store", type=float,
                default=0,
                help='Use a Beta(eta,eta) prior on strength/(1+strength)')
ap.add_argument("--sim-iterations", action="store", type=int,
                default=10**3,
                help='Number of Monte Carlo iterations per thread')
ap.add_argument("--sim-replications", action="store", type=int,
                default=4,
                help='Number of Monte Carlo threads')
ap.add_argument("--importance-gaussian", action="store_true", default=False,
                help='Use Gaussian importance sampling')
ap.add_argument("--importance-t-dof", action="store", type=int, default=-1,
                help='Number of degrees of freedom for Student-T importance sampling (0 means use #teams-1)')
ap.add_argument("--write-lambda-sims", action="store_true", default=False,
                help='Write out simulated log-strengths from Monte Carlo')
ap.add_argument("--write-importance", action="store_true", default=False,
                help='Write out weights from importance sampling')
ap.add_argument("--sim-seed", action="store", type=int,
                default=None,
                help='Seed for random number generator')
ap.add_argument("--tolerance", action="store",
                default=1e-5,
                help='Tolerance for iterative solutions')
ap.add_argument("--file-prefix", action="store",
                default="",
                help='Prefix for output filenames')
args = ap.parse_args()

## Load in the scores from the file I use for the Joe Schlobotnik/TBRW pages
scoresfile = args.scores_file

(
    date, team1, score1, team2, score2, league
) = np.loadtxt(scoresfile, unpack=True,
               dtype = [('date','<i8'),
                        ('team1','|S2'), ('score1','<i2'),
                        ('team2','|S2'), ('score2','<i2'),
                        ('league','|S2')])

## Restrict ourselves to games played between the specified dates
## Evaluation set for calculating Bayes factor
evalDatemask = np.logical_and(date >= args.first_eval_date,
                          date <= args.final_eval_date)
evalDate = date[evalDatemask]
evalTeam1 = team1[evalDatemask]
evalScore1 = score1[evalDatemask]
evalTeam2 = team2[evalDatemask]
evalScore2 = score2[evalDatemask]
evalLeague = league[evalDatemask]

## Extract the list of teams from those appearing in the results file
teams = np.unique(np.concatenate((team1,team2)))
nteams = len(teams)
teamind = dict([(t,i) for i,t in enumerate(teams)])

outfile = open(('%steams.dat' % args.file_prefix),'w')
for i in range(nteams):
    outfile.write('%s\n' % teams[i].decode('utf8'))
outfile.close()

## Restrict ourselves to games played between the specified dates
## Training set for calculating ratings
## (Do this second because it's the default)
datemask = np.logical_and(date >= args.first_training_date,
                          date <= args.final_training_date)
date = date[datemask]
team1 = team1[datemask]
score1 = score1[datemask]
team2 = team2[datemask]
score2 = score2[datemask]
league = league[datemask]

## Collect the results into head-to-head results arrays.  In order to
## do integer arithmetic as much as possible, we work in terms of
## "points for" and "points against" (2-0 for a win, 1-1 for a tie,
## 0-2 for a loss).  So h2hnum_tt is not the total number of games
## between each pair of teams, but twice that.
h2hpf_tt = np.zeros((nteams,nteams),dtype=int)

for t1, t2, s1, s2 in zip(team1, team2, score1, score2):
    if s1 > s2:
        h2hpf_tt[teamind[t1],teamind[t2]] += 2
    elif s1 < s2:
        h2hpf_tt[teamind[t2],teamind[t1]] += 2
    else:
        h2hpf_tt[teamind[t1],teamind[t2]] += 1
        h2hpf_tt[teamind[t2],teamind[t1]] += 1

#print(h2hpf_tt)
outfile = open(('%sh2hpf.dat' % args.file_prefix),'w')
for i in range(nteams):
    outfile.write(' '.join(['%g' % h2hpf for h2hpf in h2hpf_tt[i]]))
    outfile.write('\n')
outfile.close()

h2hpa_tt = h2hpf_tt.transpose()
h2hnum_tt = h2hpf_tt + h2hpa_tt

pf_t = h2hpf_tt.sum(axis=-1)
pa_t = h2hpa_tt.sum(axis=-1)

connections_tt = None
if (args.prior_sigma < np.inf):
    if (args.prior_eta > 0):
        raise ValueError('Cannot specify --prior-sigma with --prior-eta')
    lambdaMAP_t = FordMAPWithSigma(h2hpf_tt,args.prior_sigma,tol=args.tolerance)
elif (args.prior_eta > 0):
    lambdaMAP_t  = FordMAPWithEta(h2hpf_tt,args.prior_eta,tol=args.tolerance)
else:
    ## Haldane prior (MLE)
    connections_tt = findConnections(h2hpf_tt)
    lambdaMAP_t = FordMLE(h2hpf_tt,connections_tt,tol=args.tolerance)

outfile = open(('%sMAP_lambda.dat' % args.file_prefix),'w')
for i in range(nteams):
    outfile.write('%g\n' % lambdaMAP_t[i])
outfile.close()

if connections_tt is not None:
    outfile = open(('%sMAP_connections.dat' % args.file_prefix),'w')
    for i in range(nteams):
        outfile.write(' '.join(['%d' % conn for conn in connections_tt[i]]))
        outfile.write('\n')
    outfile.close()

h2hpctMAP_tt = h2hpctFromLambdas(lambdaMAP_t,connections_tt)

h2hpctPFPA_tt = 1./(1.+np.sqrt((pa_t[:,None]*pf_t[None,:])
                               /(pf_t[:,None]*pa_t[None,:])))

outfile = open(('%sMAP_h2hpct.dat' % args.file_prefix),'w')
for i in range(nteams):
    outfile.write(' '.join(['%g' % pct for pct in h2hpctMAP_tt[i]]))
    outfile.write('\n')
outfile.close()

## Bayes factor for evaluation results
log2BFMAP = 0.

outfile = open(('%sMAP_log2_BF.dat' % args.file_prefix),'w')
for t1, t2, s1, s2 in zip(evalTeam1, evalTeam2, evalScore1, evalScore2):
    if s1 > s2:
        log2BFMAP += np.log2(2.*h2hpctMAP_tt[teamind[t1],teamind[t2]])
        tW = t1
        sW = s1
        tL = t2
        sL = s2
    elif s1 < s2:
        log2BFMAP += np.log2(2.*h2hpctMAP_tt[teamind[t2],teamind[t1]])
        tW = t2
        sW = s2
        tL = t1
        sL = s1
    else:
        log2BFMAP += 0.5 * (
            np.log2(2.*h2hpctMAP_tt[teamind[t1],teamind[t2]])
            + np.log2(2.*h2hpctMAP_tt[teamind[t2],teamind[t1]])
        )
        # raise ValueError(("Tie result encountered: %s %d %s %d"
        #                   % (t1,s1,t2,s2)))
        tW = t1
        sW = s1
        tL = t2
        sL = s2
    outfile.write("%s %d %s %d %f\n"%(tW.decode('utf8'),sW,
                                      tL.decode('utf8'),sL,log2BFMAP))
outfile.close()

log2BFPFPA = 0.

outfile = open(('%sPFPA_log2_BF.dat' % args.file_prefix),'w')
for t1, t2, s1, s2 in zip(evalTeam1, evalTeam2, evalScore1, evalScore2):
    if s1 > s2:
        log2BFPFPA += np.log2(2.*h2hpctPFPA_tt[teamind[t1],teamind[t2]])
        tW = t1
        sW = s1
        tL = t2
        sL = s2
    elif s1 < s2:
        log2BFPFPA += np.log2(2.*h2hpctPFPA_tt[teamind[t2],teamind[t1]])
        tW = t2
        sW = s2
        tL = t1
        sL = s1
    else:
        log2BFPFPA += 0.5 * (
            np.log2(2.*h2hpctPFPA_tt[teamind[t1],teamind[t2]])
            + np.log2(2.*h2hpctPFPA_tt[teamind[t2],teamind[t1]])
        )
        # raise ValueError(("Tie result encountered: %s %d %s %d"
        #                   % (t1,s1,t2,s2)))
        tW = t1
        sW = s1
        tL = t2
        sL = s2
    outfile.write("%s %d %s %d %f\n"%(tW.decode('utf8'),sW,
                                      tL.decode('utf8'),sL,log2BFPFPA))
outfile.close()

## Gaussian approximation from MAP
sigsqinv_tt = - 0.5*h2hnum_tt * h2hpctMAP_tt * (1-h2hpctMAP_tt)
sigsqinv_tt -= np.diag(sigsqinv_tt.sum(axis=1))
if (args.prior_sigma < np.inf):
    sigsqinv_tt += np.diag(lambdaMAP_t/args.prior_sigma**2)
elif (args.prior_eta > 0):
    zetaMAP_t = 1./(1.+np.exp(-lambdaMAP_t))
    sigsqinv_tt += np.diag(2.*args.prior_eta*zetaMAP_t*(1.-zetaMAP_t))
sigsq_tt = np.linalg.pinv(sigsqinv_tt)
print('Check: rank of Hessian is %g, nteams is %d' %
      (np.trace(np.dot(sigsq_tt,sigsqinv_tt)),nteams))

outfile = open(('%sMAP_sigsq.dat' % args.file_prefix),'w')
for i in range(nteams):
    outfile.write(' '.join(['%g' % ssq for ssq in sigsq_tt[i]]))
    outfile.write('\n')
outfile.close()

outfile = open(('%sMAP_sigsqinv.dat' % args.file_prefix),'w')
for i in range(nteams):
    outfile.write(' '.join(['%g' % ssqinv for ssqinv in sigsqinv_tt[i]]))
    outfile.write('\n')
outfile.close()

if ((args.sim_iterations*args.sim_replications) == 0):
    exit()

Niter = args.sim_iterations
Nrep = args.sim_replications
BFSim_r = np.empty(Nrep)

## If we're not doing importance sampling, we can discard teams not
## appearing in the evaluation set
tDOF = args.importance_t_dof
if tDOF >= 0:
    sigsqRank = np.linalg.matrix_rank(sigsq_tt)
    print(sigsqRank)
    if tDOF == 0:
        tDOF = sigsqRank

if (args.importance_gaussian is False and tDOF < 0):
    ## Extract the list of teams from those appearing in the evaluation set
    evalTeams_e = np.unique(np.concatenate((evalTeam1,evalTeam2)))
    evalNteams = len(evalTeams_e)
    ## Indices in original list of teams in evaluation set
    evalOriginds_e = [teamind[e] for e in evalTeams_e]
    evalTeamind = dict([(t,i) for i,t in enumerate(evalTeams_e)])
    evalLambdaMAP_e = lambdaMAP_t[evalOriginds_e]
    evalSigsq_ee = sigsq_tt[evalOriginds_e][:,evalOriginds_e]
    if connections_tt is None:
        evalConnections_ee = None
    else:
        evalConnections_ee = connections_tt[evalOriginds_e][:,evalOriginds_e]
    lambdaMAP_t = evalLambdaMAP_e
    sigsq_tt = evalSigsq_ee
    connections_tt = evalConnections_ee
    teamind = evalTeamind
    importanceFlag = False
else:
    importanceFlag = True
    BFImp_r = np.empty(Nrep)
    if args.write_importance:
        lnimpnumerfile = open(('%slnimpnumer.dat' % args.file_prefix),'w')
        lnimpdenomfile = open(('%slnimpdenom.dat' % args.file_prefix),'w')

if args.sim_seed is not None:
    np.random.seed(args.sim_seed)

if args.write_lambda_sims:
    lambdasimfile = open(('%slambdasim.dat' % args.file_prefix),'w')

for j in range(Nrep):
    BFSim = 0.
    BFImp = 0.
    Nimp = 0.
    for i in range(Niter):
        if tDOF > 0:
            sigsqMult = 1 + (sigsqRank-1) / tDOF
            lambdasim_t = (
                lambdaMAP_t
                + (
                    np.random.multivariate_normal(np.zeros(nteams),
                                                  sigsqMult*sigsq_tt)
                    / np.sqrt(stats.gamma(0.5*tDOF,
                                          scale=2./tDOF).rvs(size=nteams))
                )
            )
        else:
            lambdasim_t = np.random.multivariate_normal(lambdaMAP_t,sigsq_tt)

        h2hpctsim_tt = h2hpctFromLambdas(lambdasim_t,connections_tt)

        if args.write_lambda_sims is True:
            lambdasimfile.write(' '.join([('%g' % lam)
                                           for lam in lambdasim_t]))
            lambdasimfile.write('\n')

        log2BFSim = 0.

        for t1, t2, s1, s2 in zip(evalTeam1, evalTeam2,
                                   evalScore1, evalScore2):
            if s1 > s2:
                log2BFSim += np.log2(2.*h2hpctsim_tt[teamind[t1],
                                                    teamind[t2]])
            elif s1 < s2:
                log2BFSim += np.log2(2.*h2hpctsim_tt[teamind[t2],
                                                    teamind[t1]])
            else:
                log2BFSim += 0.5 * (
                    np.log2(2.*h2hpctsim_tt[teamind[t1],
                                         teamind[t2]])
                    + np.log2(2.*h2hpctsim_tt[teamind[t2],
                                           teamind[t1]])
                )
                # raise ValueError(("Tie result encountered: %s %d %s %d"
                #                   % (t1,s1,t2,s2)))

        if importanceFlag is True:
            lnimpnumer = 0.5*np.sum(h2hpf_tt*np.log(h2hpctsim_tt/h2hpctMAP_tt))
            lnimpdenom = -0.5*np.dot((lambdasim_t-lambdaMAP_t),
                                     np.dot(sigsqinv_tt,
                                            (lambdasim_t-lambdaMAP_t)))
            if tDOF > 0:
                lnimpdenom = -0.5*(tDOF+sigsqRank) * np.log (
                    1. - (2./tDOF) * lnimpdenom / sigsqMult
                )

            if args.write_importance is True:
                lnimpnumerfile.write('%g' % lnimpnumer)
                lnimpnumerfile.write('\n')
                lnimpdenomfile.write('%g' % lnimpdenom)
                lnimpdenomfile.write('\n')

            impratio = np.exp(lnimpnumer-lnimpdenom)
            BFImp += impratio*2**log2BFSim
            Nimp += impratio

        BFSim += 2**log2BFSim

    BFSim_r[j] = BFSim / Niter

    if importanceFlag is True:
        BFImp_r[j] = BFImp / Nimp

if args.write_lambda_sims:
    lambdasimfile.close()

outfile = open(('%smonte_log2_BF.dat' % args.file_prefix),'w')
outfile.write(' '.join([('%g' % np.log2(B)) for B in BFSim_r]))
outfile.write('\n')
outfile.close()

if importanceFlag is True:
    if args.write_importance:
        lnimpnumerfile.close()
        lnimpdenomfile.close()
    outfile = open(('%simp_log2_BF.dat' % args.file_prefix),'w')
    outfile.write(' '.join([('%g' % np.log2(B)) for B in BFImp_r]))
    outfile.write('\n')
    outfile.close()
