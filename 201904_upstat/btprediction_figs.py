#!/usr/bin/env python3
#
# Copyright (C) 2020 John T. Whelan
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
This script generates most of the plots for Whelan and Wodon,
"Prediction and Evaluation in College Hockey Using the
Bradley-Terry-Zermelo Model" (2020), to appear in Mathematics for
Applications.  Additional plots are created by elfdemo_figs.py
"""

__author__ = "John Whelan <jtw24@cornell.edu>"

import matplotlib
matplotlib.use("Agg")
from matplotlib import rc
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif')
import matplotlib.pyplot as plt
import matplotlib.font_manager as ftmgr
import numpy as np
from scipy.special import expit, logit
from scipy import stats

gausslnimpnumer_I = np.loadtxt('gaussimpratio_lnimpnumer.dat',unpack=True)
gausslnimpdenom_I = np.loadtxt('gaussimpratio_lnimpdenom.dat',unpack=True)
gaussweights_I = np.exp(gausslnimpnumer_I-gausslnimpdenom_I)
gaussweights_I /= np.sum(gaussweights_I)
Niter = len(gaussweights_I)

counts,bins = np.histogram(gaussweights_I,bins=100)
widths = bins[1:]-bins[:-1]

fig = plt.figure()

fig.set_size_inches(5,3)

ax = fig.add_subplot(111)
ax.bar(bins[:-1],counts,widths,color='b',ec='k',fc=[0.7,0.7,0.7])
ymax=ax.get_ylim()[-1]
xmax=ax.get_xlim()[-1]
ax.plot([1./Niter,1./Niter],[0,ymax],'k--')
ax.grid(True)
ax.set_xlim([0-0.5*widths[0],xmax])
ax.set_ylim([0,ymax])
ax.set_xlabel('Weight')
ax.set_ylabel(r'\#')
ax.set_title('Importance Sampling with Gaussian Distribution')

fig.savefig('gaussimpratiohist.eps',bbox_inches='tight')

# Don't use unpack=True on matrices since it transposes them!
lambdasim_It = np.loadtxt('gaussimpratio_lambdasim.dat')
lambdaMLE_t = np.loadtxt('gaussimpratio_MAP_lambda.dat',unpack=True)
sigsqinv_tt = np.loadtxt('gaussimpratio_MAP_sigsqinv.dat',unpack=True)
sigsq_tt = np.loadtxt('gaussimpratio_MAP_sigsq.dat',unpack=True)
h2hpf_tt = np.loadtxt('gaussimpratio_h2hpf.dat')
# h2hpctMLE_tt = np.loadtxt('gaussimpratio_MAP_h2hpct.dat')
badI = np.argmax(gaussweights_I)

badlambda_t = lambdasim_It[badI]
badlambdaoffset_t = badlambda_t-lambdaMLE_t
badlambdadistance = np.linalg.norm(badlambdaoffset_t)
badunit_t = badlambdaoffset_t/badlambdadistance
lambdaMLEcmpt = np.dot(badunit_t,lambdaMLE_t)
lambdacmpt_l = lambdaMLEcmpt + np.linspace(-1.1,1.1,2201)*badlambdadistance
sigsqinv = np.dot(badunit_t,np.dot(sigsqinv_tt,badunit_t))
h2hpctMLE_tt = expit(lambdaMLE_t[:,None]-lambdaMLE_t[None,:])

z_l = (lambdacmpt_l - lambdaMLEcmpt)*np.sqrt(sigsqinv)
gausslnpost_l = -0.5*z_l**2

tDOF = 59
# sigsqRank = np.linalg.matrix_rank(sigsq_tt)
sigsqRank = 59
sigsqMult = 1 + (sigsqRank-1) / tDOF
print(sigsqRank)
tlnpost_l = -0.5*(tDOF+sigsqRank) * np.log (
    1. - (2./tDOF) * gausslnpost_l / sigsqMult
)

lambda_lt = (
    lambdaMLE_t[None,:]
    + ( lambdacmpt_l[:,None] - lambdaMLEcmpt ) * badunit_t[None,:]
)
h2hpct_ltt = expit(lambda_lt[:,:,None]-lambda_lt[:,None,:])
lnpost_l = 0.5*np.sum(h2hpf_tt[None,:,:]
                      * np.log(h2hpct_ltt/h2hpctMLE_tt[None,:,:]),
                      axis=(1,2))

fig = plt.figure()

fig.set_size_inches(5,3)

ax = fig.add_subplot(111)

ax.plot(lambdacmpt_l,lnpost_l,'k-')
ax.plot(lambdacmpt_l,gausslnpost_l,'r--')
ax.plot(lambdacmpt_l,tlnpost_l,'g-.')
ax.plot(lambdaMLEcmpt+badlambdadistance,gausslnimpnumer_I[badI],'ko-',
        label='Exact')
ax.plot(lambdaMLEcmpt+badlambdadistance,gausslnimpdenom_I[badI],'rs--',
        label='Gaussian')
ax.plot(lambdaMLEcmpt+badlambdadistance,tlnpost_l[2101],'gd-.',
        label=r'Student-$t$')
# ax.legend(numpoints=1,handlelength=3.5)
ax.legend(handlelength=4.1)
ax.set_title('Worst-Case Cross-Section Through Posterior')
ax.set_xlabel(r'$\sum_{i=1}^{t}\lambda_i\, u_i$')
ax.set_ylabel(r'$\ln f(\{\lambda_i\}|D,I)-\ln f(\{\widehat{\lambda}_i\}|D,I)$')

ax.grid(True)

xmin = np.min(lambdacmpt_l)
xmax = np.max(lambdacmpt_l)
ax.set_xlim([xmin,xmax])
ax2 = ax.twiny()
ax2.set_xlim([np.min(z_l),np.max(z_l)])
ax2.set_xlabel(r'$\sqrt{\sum_{i=1}^{t}\sum_{j=1}^{t}(\lambda_i-\widehat{\lambda}_i)H_{ij}(\lambda_j-\widehat{\lambda}_j)}$')

fig.savefig('crosssection.eps',bbox_inches='tight')

lamdist_l = np.linspace(0,12,1201)
fig = plt.figure()

fig.set_size_inches(5,3)
ax = fig.add_subplot(111)
ax.plot(lamdist_l,stats.chi(df=sigsqRank).pdf(lamdist_l),'k-')
ax.set_xlabel(r'$\sqrt{\sum_{i=1}^{t}\sum_{j=1}^{t}(\lambda_i-\widehat{\lambda}_i)H_{ij}(\lambda_j-\widehat{\lambda}_j)}$')
ax.set_ylabel('Probability Density')
ax.set_title('Posterior distance distribution (Gaussian approx)')
ax.set_xlim([0,12])
ax.set_ylim([0,0.6])
ax.set_yticks([])
ax.grid(True)

fig.savefig('chi59dens.eps',bbox_inches='tight')

(teamW2019,scoreW2019,teamL2019,scoreL2019
) = np.loadtxt('Haldane_2019_MAP_log2_BF.dat',
               unpack=True,
               dtype = [('teamW','|S2'), ('scoreW','<i2'),
                        ('teamL','|S2'), ('scoreL','<i2')],
               usecols=(0,1,2,3))

MAPlog2BF2019_g = np.loadtxt('Haldane_2019_MAP_log2_BF.dat',
                              unpack=True,usecols=(4,))

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)

xvals_G = range(-1,15)
ax.plot(xvals_G,np.concatenate(([0.,],MAPlog2BF2019_g)),'ko-')
ax.set_xticks(xvals_G)
ax.set_xticklabels(['',]+[('%s def %s' % (tW.decode('utf8'),tL.decode('utf8')))
                        for (tW,tL) in zip(teamW2019,teamL2019)],
                   rotation=45,ha='right')
ax.set_xlim([-1.5,14.5])
tickvals = np.arange(-3,4)
ticklabs = ['%g' % 2.**x for x in tickvals]
ax.set_yticks(tickvals)
ax.set_yticklabels(ticklabs)
ax.grid(True)
ax.set_title('Bayes factor for KRACH in 2019 NCAAs')
ax.set_ylabel('Cumulative Bayes factor vs tossup')
fig.savefig('KRACH2019BF.eps',bbox_inches='tight')

years = np.arange(2003,2020)

HaldaneMAPlog2BF_y = np.empty(len(years))

for i in range(len(years)):
    HaldaneMAPlog2BF_y[i] = np.loadtxt(
        ('Haldane_%d_MAP_log2_BF.dat' % years[i]),
        unpack=True,usecols=(4,)
    )[-1]

Nrep = 4
Laplaceimplog2BF_yj = np.empty((len(years),Nrep))
Laplacemontelog2BF_yj = np.empty((len(years),Nrep))
LaplaceMAPlog2BF_y = np.empty(len(years))
for i in range(len(years)):
    Laplaceimplog2BF_yj[i] = np.loadtxt(
        ('Laplace_%d_imp_log2_BF.dat' % years[i])
    )
    Laplacemontelog2BF_yj[i] = np.loadtxt(
        ('Laplace_%d_monte_log2_BF.dat' % years[i])
    )
    LaplaceMAPlog2BF_y[i] = np.loadtxt(
        ('Laplace_%d_MAP_log2_BF.dat' % years[i]),
        unpack=True,usecols=(4,)
    )[-1]

Laplacemuimplog2BF_y = Laplaceimplog2BF_yj.mean(axis=-1)
Laplacedimplog2BF_y = Laplaceimplog2BF_yj.std(ddof=1,axis=-1)/np.sqrt(Nrep)
Laplacemumontelog2BF_y = Laplacemontelog2BF_yj.mean(axis=-1)
Laplacedmontelog2BF_y = Laplacemontelog2BF_yj.std(ddof=1,axis=-1)/np.sqrt(Nrep)

print(Laplacemuimplog2BF_y)
print(Laplacedimplog2BF_y)
print(Laplacemumontelog2BF_y)
print(Laplacedmontelog2BF_y)

PFPAlog2BF_y = np.empty(len(years))

for i in range(len(years)):
    PFPAlog2BF_y[i] = np.loadtxt(
        ('Laplace_%d_PFPA_log2_BF.dat' % years[i]),
        unpack=True,usecols=(4,)
    )[-1]

fig = plt.figure();
fig.set_size_inches(5,3);

ax = fig.add_subplot(111)

xvals_Y = np.concatenate(([years[0]-1],years))
ax.plot(xvals_Y,np.concatenate(([0.,],np.cumsum(HaldaneMAPlog2BF_y))),'ko-',
        label=r'KRACH')
# ax.plot(xvals_Y,np.concatenate(([0.,],np.cumsum(LaplaceMAPlog2BF_y))),'gd-.',
#         label=r'$\eta=1$')
for j in range(Nrep):
    if (j==0):
        ax.plot(xvals_Y,
                np.concatenate(([0.,],np.cumsum(Laplacemontelog2BF_yj[:,j]))),
                'gd-.',
                label=r'$\eta=1$')
    else:
        ax.plot(xvals_Y,
                np.concatenate(([0.,],np.cumsum(Laplacemontelog2BF_yj[:,j]))),
                'gd-.')
# for j in range(Nrep):
#     if (j==0):
#         ax.plot(xvals_Y,
#                 np.concatenate(([0.,],np.cumsum(Laplaceimplog2BF_yj[:,j]))),
#                 'gd-.',lw=0.5,mfc='None'
#                 ,label=r'importance sampling'
#         )
#     else:
#         ax.plot(xvals_Y,
#                 np.concatenate(([0.,],np.cumsum(Laplaceimplog2BF_yj[:,j]))),
#                 'gd-.',lw=0.5,mfc='None')
ax.plot(xvals_Y,np.concatenate(([0.,],np.cumsum(PFPAlog2BF_y))),'rs--',
        label=r'Win ratio')
ax.legend(loc=(0.7,0),
          prop=ftmgr.FontProperties(size=10),
          handlelength=3.5,
          numpoints=2)
ax.set_xticks(xvals_Y)
ax.set_xticklabels(['',]+[('%d' % year)
                          for year in years],
                   rotation=90)
ax.set_xlim(years[0]-1.5,years[-1]+0.5)
tickvals = np.arange(0,20,2)
ticklabs = ['%g' % 2**x for x in tickvals]
ax.set_yticks(tickvals)
ax.set_yticklabels(ticklabs)
ax.grid(True)
ax.set_title('Bayes factor in NCAAs')
ax.set_ylabel('Cumulative Bayes factor vs tossup')
fig.savefig('NCAABF.eps',bbox_inches='tight')

datfile = open('btprediction_data.tex','w')
datfile.write(r'\newcommand{\badgaussdist}{%.3f}'
              % (badlambdadistance*np.sqrt(sigsqinv))
              )
datfile.write('\n')
datfile.close()
